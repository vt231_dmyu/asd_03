#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <math.h>

double factorial(int n) {
    if (n == 0)
        return 1;
    else
        return n * factorial(n - 1);
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

    printf("n\tf(n) = n\tf(n) = log(n)\tf(n) = n*log(n)\tf(n) = n^2\tf(n) = 2^n\tf(n) = n!\n");
    for (int n = 0; n <= 50; n++) {
        printf("%d\t", n);
        printf("%10d\t", n);
        if (n > 0)
            printf("%10.2f\t", log(n));
        else
            printf("undefined\t");

        if (n > 0)
            printf("%10.2f\t", n * log(n));
        else
            printf("undefined\t");

        printf("%10d\t", n * n);
        printf("%10.2f\t", pow(2, n));
        printf("%10.0f\t\n", factorial(n));
    }

	return 0;
}