﻿#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <iostream>
#include <chrono>
#define GETTIME std::chrono::steady_clock::now
#define CALCTIME std::chrono::duration_cast<std::chrono::nanoseconds>

char* intToBinaryString(int a, int b) {
	char* binary = (char*)malloc(b + 1);
	binary[b] = '\0';

	for (int i = b - 1; i >= 0; i--) {
		binary[i] = (a & 1) ? '1' : '0'; // Отримуємо найменший розряд числа та перетворюємо його в символ '1' або '0'
		a >>= 1; // Зсуваємо число на один біт вправо
	}

	return binary;
}

int fibonacci(int n) {
	if (n <= 1)
		return n;
	else
		return fibonacci(n - 1) + fibonacci(n - 2);
}


int main() {
	system("chcp 1251"); // Перехід в консолі на кирилицю
	system("cls"); // Очищаємо вікно консолі

	int a, b_values[57], n;
	for (int i = 0; i < 57; i++)
		b_values[i] = 8 + i;

	printf("№9 Дано ціле число a з розрядністю b, де 8<=b<=64. Реалізувати функцію, яка повертає двійкове представлення числа a у вигляді строки.\n");
	printf("Введіть a: "); scanf("%d", &a);

	for (int i = 0; i < 57; i++) {
		auto begin = GETTIME();
		char* binary = intToBinaryString(a, b_values[i]);
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);
		printf("Розрядність b: %d, Двійкове представлення: %s, Час виконання: %lld нс.\n", b_values[i], binary, spent.count());
		free(binary);
	}

	printf("\n№6 Реалізувати функцію обчислення n-го числа Фібоначчі за допомогою рекурсії, де n<=40, n – ціле число. Обраховуються числа Фібоначчі за виразом: F0 = 0, F1 = 1, Fn = Fn-1 + Fn-2, де n>=2.\n");
	printf("Введіть значення n (n <= 40): "); scanf("%d", &n);

	if (n > 40) {
		printf("n повинно бути менше або рівне 40!\n");
		return 1;
	}

	for (int i = n; i <= 40; i++) {
		auto begin = GETTIME();
		int result = fibonacci(i);
		auto end = GETTIME();
		auto spent = CALCTIME(end - begin);
		printf("Значення %d-го числа Фібоначчі: %d. Час виконання: %lld нc.\n", i, result, spent.count());
	}

	return 0;
}